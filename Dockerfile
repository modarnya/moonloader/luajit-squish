FROM ubuntu:latest

RUN apt-get update \
    && apt-get install -y git lua5.1 luajit make --no-install-recommends \
    && apt-get clean -y \
    && git config --global http.sslverify false \
    && git clone -q https://github.com/LuaDist/squish \
    && cd squish \
    && make install \
    && cp make_squishy /usr/local/bin/make_squishy \
    && chmod +x /usr/local/bin/make_squishy \
    && cd \